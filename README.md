# broker 

A message broker for HypeDeals using AMQPLibrary.

# Installation

To perform package installation, type a following syntax in your terminal.

> php composer require trego/broker

Then copy arrays inside trego/broker/amqp.php inside vendor directory to config 
directory in main directory. Adjust config file base on your configuration.

# Post Installation

After completed an installation, add the following syntax in Register Service 
Provider section at bootstrap/app.php file.

```php
$app->register(Trego\Broker\BrokerServiceProvider::class)
```

Then added the following syntax to configure a config file at bootstrap/app.php
file.

```php
// $app->withFacades(); 

// $app->withEloquent();

$app->configure('amqp');
```

# How to use

After perform such installation, you are now able to Publish and Consume a 
message.

To publish a message 

```php
use Trego\Broker\Facades\Broker;

Broker::publish('message', 'routing-key')
```

To consume a message

```php
use Trego\Broker\Facades\Broker;

Broker::consume('queue-name', 'routing-key', function ($message, $consumer) {
    var_dump($message->body);
    $consumer->acknowledge($message);
});
```
    
