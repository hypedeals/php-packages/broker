<?php

namespace Trego\Broker\Test;

use Broker;

class PublisherTest extends BaseTestCase
{
    public function testPublishMessage()
    {
        $test = Broker::publish('test', 'trego.test');
        $this->assertTrue($test);
    }
}
