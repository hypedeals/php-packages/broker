<?php

namespace Trego\Broker\Test;

use Broker;

class ConsumerTest extends BaseTestCase
{
    public function testConsume()
    {
        $consume = Broker::consume('User Registration', 'trego.test', function ($message, $resolver) {
            $resolver->acknowledge($message);
            $resolver->stopWhenProcessed();
        });

        $this->assertTrue($consume);
    }

    public function testGetRoutingKeyAfterConsume()
    {
        $consume = Broker::consume('test', 'trego.test', function ($message, $resolver) {
            $resolver->acknowledge($message);
            $routing_key = $resolver->getRoutingKey($message);
            $resolver->stopWhenProcessed();
        });

        $this->assertEquals('trego.test', $routing_key);
    }

    public function testRejectConsume()
    {
        $consume = Broker::consume('', 'test', function ($message, $resolver) {
            $resolver->acknowledge($message);
            $resolver->reject($message);
        });

        $this->assertTrue($consume);
    }
}
