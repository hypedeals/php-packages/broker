<?php

namespace Trego\Broker\Facades;

use Illuminate\Support\Facades\Facade;
use Trego\Broker\BrokerService;

class Broker extends Facade
{
    /**
     * Get a registered name of component.
     */
    protected static function getFacadeAccessor()
    {
        return BrokerService::class;
    }
}
