<?php

namespace Trego\Broker;

use Illuminate\Support\ServiceProvider;
use Trego\Broker\Requests\Publisher;
use Trego\Broker\Requests\Consumer;
use Illuminate\Config\Repository;

class BrokerServiceProvider extends ServiceProvider
{
    /**
     * Indicated if loading provicder is deffered.
     *
     * @var bool
     */
    protected $defer = true;

    public function boot()
    {
    }

    /**
     * Register binding in the container.
     */
    public function register()
    {
        $config = new Repository($this->app->config->get('amqp'));

        $this->app->singleton(BrokerService::class, function () use ($config) {
            $publisher = new Publisher($config);
            $consumer = new Consumer($config);

            return new BrokerService($publisher, $consumer);
        });

        if (!class_exists('Broker')) {
            class_alias('Trego\Broker\Facades\Broker', 'Broker');
        }
    }

    public function provides()
    {
        return [
            'Broker',
        ];
    }
}
