<?php

namespace Trego\Broker;

use Trego\Broker\Requests\Message;
use Trego\Broker\Requests\Publisher;
use Trego\Broker\Requests\Consumer;
use Closure;

class BrokerService
{
    /**
     * @var Publisher
     */
    protected $publisher;

    /**
     * @var Consumer
     */
    protected $consumer;

    public function __construct(Publisher $publisher, Consumer $consumer)
    {
        $this->publisher = $publisher;
        $this->consumer = $consumer;
    }

    /**
     * Publish a message.
     *
     * @param string $routing
     * @param string $message
     * @param array  $properties
     *
     * @return string $tags
     */
    public function publish($message, $routing, array $properties = [])
    {
        try {
            $msg = new Message($message);
            $this->publisher->initiatePublish();
            $this->publisher->publish($msg, $routing);
            $this->publisher->shutdown($this->publisher->getChannel(), $this->publisher->getConnection());

            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Consume a message.
     *
     * @param string  $queue
     * @param Closure $closure
     * @param array   $properties
     */
    public function consume($queue, $bind_key, Closure $closure, array $properties = [])
    {
        try {
            $this->consumer->initiateConsume($queue, $bind_key);
            $this->consumer->consume($closure);
            $this->consumer->shutdown($this->consumer->getChannel(), $this->consumer->getConnection());

            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
