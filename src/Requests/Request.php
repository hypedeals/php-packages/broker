<?php

namespace Trego\Broker\Requests;

use Trego\Broker\Repositories\ConfigRepository;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Exception;
use PhpAmqpLib\Channel\AMQPChannel;

class Request extends ConfigRepository
{
    /**
     * @var AMQPStreamConnection
     */
    protected $connection;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    /**
     * @var string
     */
    protected $queue;

    /**
     * Connect to stream connection.
     */
    public function connect()
    {
        $this->connection = new AMQPStreamConnection(
            $this->getConfig('host'),
            $this->getConfig('port'),
            $this->getConfig('username'),
            $this->getConfig('password'),
            $this->getConfig('vhost')
        );

        $this->channel = $this->connection->channel();

        return true;
    }

    /**
     * Initiate the declare.
     */
    public function initiatePublish()
    {
        $this->connect();

        $exchange = $this->getConfig('exchange');

        if (is_null($exchange)) {
            throw new Exception\Configuration('Exchange type has not defined!');
        }

        $this->channel->exchange_declare(
            $exchange,
            $this->getConfig('exchange_type'),
            $this->getConfig('exchange_passive'),
            $this->getConfig('exchange_durable'),
            $this->getConfig('exchange_auto_delete')
        );
    }

    public function initiateConsume($queue, $bind = '')
    {
        $this->connect();

        $exchange = $this->getConfig('exchange');

        if (is_null($exchange)) {
            throw new Exception\Configuration('Exchange type has not been defined');
        }

        $this->channel->exchange_declare(
            $exchange,
            $this->getConfig('exchange_type'),
            $this->getConfig('exchange_passive'),
            $this->getConfig('exchange_durable'),
            $this->getConfig('exchange_auto_delete')
        );

        if (!is_null($queue)) {
            list($this->queue) = $this->channel->queue_declare(
                $queue,
                $this->getConfig('queue_passive'),
                $this->getConfig('queue_durable'),
                $this->getConfig('queue_exclusive'),
                $this->getConfig('queue_auto_delete'),
                $this->getConfig('queue_no_wait'),
                $this->getConfig('queue_arguments')
            );

            $this->channel->queue_bind(
                $queue,
                $exchange,
                $bind ?: '#'
            );
        }

        $this->connection->set_close_on_destruct(true);
    }

    /**
     * Shutdown both channel and connection.
     *
     * @param AMQPChannel    $channel
     * @param AMQPConnection $connection
     */
    public function shutdown(AMQPChannel $channel, AMQPStreamConnection $connection)
    {
        $channel->close();
        $connection->close();
    }

    /**
     * retun a channel.
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * return a connection.
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Get a message number.
     */
    public function getMessageCount()
    {
        if (is_array($this->queue)) {
            return $this->queue[0];
        }

        return 0;
    }
}
