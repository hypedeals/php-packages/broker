<?php

namespace Trego\Broker\Requests;

class Publisher extends Request
{
    public function publish(Message $message, $routing)
    {
        return $this->getChannel()->basic_publish($message, $this->getConfig('exchange'), $routing);
    }
}
