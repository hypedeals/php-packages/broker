<?php

return [
    'host' => env('RABBITMQ_HOST', 'toad-01.rmq.cloudamqp.com'),
    'port' => env('RABBITMQ_PORT', 5672),
    'username' => env('RABBITMQ_USERNAME', 'ycrqsmcr'),
    'password' => env('RABBITMQ_PASSWORD', 'Y9zDaI0N9pvKBS9g7YBtzPeX66slxGM3'),
    'vhost' => env('RABBITMQ_VHOST', 'ycrqsmcr'),

    'exchange' => env('RABBITMQ_EXCHANGE', 'amq.topic'),
    'exchange_type' => 'topic',
    'exchange_passive' => false,
    'exchange_durable' => true,
    'exchange_auto_delete' => false,
    'exchange_internal' => false,
    'exchange_no_wait' => false,
    'exchange_arguments' => [],

    'queue' => '',
    'queue_passive' => false,
    'queue_durable' => true,
    'queue_exclusive' => false,
    'queue_auto_delete' => false,
    'queue_wait' => false,
    'queue_arguments' => [],

    'consumer_tag' => '',
    'consumer_no_local' => false,
    'consumer_no_ack' => false,
    'consumer_exclusive' => false,
    'consumer_no_wait' => false,
    'timeout' => 0,
    'persistent' => false,
];
